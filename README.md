# [PUCRS - Redes de Comunicação II - TF](https://gitlab.com/debemdeboas/pucrs-redes-de-comunicacao-ii-tf)

Esse trabalho visa realizar um ataque de DNS spoofing utilizando um
servidor DHCP malicioso e um servidor DNS malicioso, realizando
um redirecionamento de um hostname/site para outro IP.
Por exemplo, o usuário pode pensar que está acessando o site do
Facebook, mas na realidade está acessando uma cópia do site rodando
na máquina do atacante, que agora já possui todos os dados que a vítima
enviou.

Os códigos estão brevemente documentados em seus arquivos-fonte.
A maior fonte de documentação está no arquivo [`main.py`](./main.py).

A pasta `reference` contém alguns dos arquivos utilizados
no desenvolvimento desse trabalho. Por exemplo a seguinte imagem:

![DHCP flow](reference/Figure%201.%20IP%20address%20allocation%20lease%20procedure%20using%20DHCP1.gif)

## Setup e inicialização

~~Servidor DNS utilizado: [Bind9](https://www.isc.org/bind/). Estamos utilizando sua [imagem Docker](https://hub.docker.com/r/internetsystemsconsortium/bind9).~~
Servidor DNS utilizado é DNSMasq por sua simplicidade.

Para rodar o sistema como um todo devemos realizar alguns passos.
Primeiro, devemos configurar o ambiente:

1. Instalar Docker, preferencialmente o programa [Docker Desktop](https://www.docker.com/products/docker-desktop/)
2. Instalar Docker Compose:

```shell
$ sudo apt install docker-compose-plugin
```

Agora podemos rodar o sistema.
Para isso, devemos rodar o `docker compose` da seguinte forma:

```shell
$ sudo docker compose up --build
```

**Ênfase no `sudo` para podermos alocar a porta 53**[^1].
Isso levantará dois containers: o servidor DHCP (escrito por mim), e o servidor DNS (dnsmasq).

[^1]: Na realidade, `sudo` só é necessário se estivermos usando o driver de rede
`host`. Originalmente era o que usávamos, mas estamos usando agora
o driver `bridge`. Ou seja, não é mais necessário.

Para testarmos o servidor DHCP devemos subir mais um container,
um cliente que utilizará nosso servidor.
Para isso vamos subir um contairer Alpine (uma distribuição Linux
extremamente leve) e conectá-lo à rede que foi criada pelo Compose.
Faça isso em outro terminal.

```shell
$ docker run -it --privileged --network redes-ii-tf alpine
$ /
```

Esse comando nos colocará dentro da imagem Alpine.
Devemos adicionar o pacote `dhclient` para podermos
interagir com o servidor DHCP. Como estamos em um container
Docker precisamos atualizar o arquivo `/etc/resolv.conf` manualmente.

```shell
$ / apk add dhclient
$ / dhclient -r eth0 && dhclient eth0 -v && cat /etc/resolv.conf.dhclient-new.* > /etc/resolv.conf
$ / cat /etc/resolv.conf.dhclient-new.* > /etc/resolv.conf
```

Podemos ver nas mensagens dos comandos que o processo
de DHCP ocorreu sem problemas.

## DNS spoofing

O nosso servidor de DNS malicioso mapeia os seguintes hostnames
para IPs diferentes dos originais:

| Hostname                                |   IP original    |   IP malicioso   | Hostname destino                                    |
| :-------------------------------------- | :--------------: | :--------------: | :-------------------------------------------------- |
| [youtube.com](https://www.youtube.com/) | `142.251.128.46` | `172.65.251.78`  | [gitlab.com](https://www.gitlab.com/)               |
| [google.com](https://www.google.com/)   | `104.18.20.134`  | `50.223.129.200` | [rfc-editor.org](https://www.rfc-editor.org/)       |
| [pucrs.br](https://www.pucrs.br/)       | `142.251.128.78` | `151.101.193.69` | [stackoverflow.com](https://www.stackoverflow.com/) |

Esse comportamento pode ser verificado em nosso container Alpine:

```shell
$ / ping pucrs.br
PING pucrs.br (104.18.20.134): 56 data bytes
$ / dhclient -r eth0 && dhclient eth0 -v && cat /etc/resolv.conf.dhclient-new.* > /etc/resolv.conf
$ / ping pucrs.br
PING pucrs.br (50.223.129.200): 56 data bytes
```

Um exemplo de execução segue:

![Exemplo de execução](img/system-evidence.png)
