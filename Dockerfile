FROM python:3.11-alpine

WORKDIR /app

RUN apk add --update build-base linux-headers

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY main.py /app/main.py
COPY source /app/source

USER root

CMD ["python", "-u", "main.py"]
