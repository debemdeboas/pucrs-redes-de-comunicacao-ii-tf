
import math
import socket

from ctypes import BigEndianStructure, c_uint, c_uint16, c_uint32, memmove, pointer, sizeof, c_ubyte
from ipaddress import IPv6Address
from random import randint
from typing import Dict, Tuple


class TCPHeader(BigEndianStructure):
    """
    RFC793. Represents a TCP header.

    ```
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |          Source Port          |       Destination Port        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                        Sequence Number                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Acknowledgment Number                      |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  Data |           |U|A|P|R|S|F|                               |
    | Offset| Reserved  |R|C|S|S|Y|I|            Window             |
    |       |           |G|K|H|T|N|N|                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |           Checksum            |         Urgent Pointer        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Options                    |    Padding    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                             data                              |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ```
    """
    _fields_ = [
        ('source_port', c_uint16),
        ('destination_port', c_uint16),
        ('sequence_number', c_uint32),
        ('ack_number', c_uint32),
        ('offset', c_uint16, 4),
        ('reserved', c_uint16, 3),
        # Control Bits:  6 bits (from left to right):
        # URG:  Urgent Pointer field significant
        # ACK:  Acknowledgment field significant
        # PSH:  Push Function
        # RST:  Reset the connection
        # SYN:  Synchronize sequence numbers
        # FIN:  No more data from sender
        ('non', c_uint16, 1),
        ('cwr', c_uint16, 1),
        ('ecn', c_uint16, 1),
        ('urg', c_uint16, 1),
        ('ack', c_uint16, 1),
        ('psh', c_uint16, 1),
        ('rst', c_uint16, 1),
        ('syn', c_uint16, 1),
        ('fin', c_uint16, 1),
        ('window', c_uint16),
        ('checksum', c_uint16),
        ('urgent_pointer', c_uint16),
    ]


class TCPFlagHeader(BigEndianStructure):
    _fields_ = [
        ('kind', c_ubyte),
        ('len', c_ubyte),
    ]


def pack_tcp_header(ip_info: Tuple[IPv6Address, IPv6Address, int],
                    src_port: int,
                    dst_port: int,
                    opts: bytes,
                    data: bytes = b'',
                    seq_num: int = 0,
                    non: int = 0,
                    cwr: int = 0,
                    ecn: int = 0,
                    urg: int = 0,
                    ack: int = 0,
                    psh: int = 0,
                    rst: int = 0,
                    syn: int = 0,
                    fin: int = 0,
                    window: int = 0xffc4,
                    urg_p: int = 0) -> TCPHeader:
    header = TCPHeader()
    header.source_port = src_port
    header.destination_port = dst_port
    header.sequence_number = seq_num
    header.ack_number = 0
    header.offset = 5 + (math.ceil(len(opts) / 4))
    header.reserved = 0
    header.non = non
    header.cwr = cwr
    header.ecn = ecn
    header.urg = urg
    header.ack = ack
    header.psh = psh
    header.rst = rst
    header.syn = syn
    header.fin = fin
    header.window = window
    header.urgent_pointer = urg_p

    header.checksum = calculate_tcp_checksum((ip_info[0], ip_info[1], sizeof(header) + len(opts) + len(data)), header, opts, data)
    return header


def calculate_tcp_checksum(ip_info: Tuple[IPv6Address, IPv6Address, int], tcp_header: TCPHeader, opts: bytes, data: bytes) -> int:
    class TCPChecksumPseudoHeader(BigEndianStructure):
        _fields_ = [
            ('src_ip', c_ubyte * 16),
            ('dst_ip', c_ubyte * 16),
            ('len', c_uint32),
            ('zero', c_uint, 24),
            ('next_header', c_ubyte, 8)
        ]

    checksum_header = TCPChecksumPseudoHeader()
    checksum_header.src_ip = (c_ubyte * 16)(*list(ip_info[0].packed))
    checksum_header.dst_ip = (c_ubyte * 16)(*list(ip_info[1].packed))
    checksum_header.len = ip_info[2]
    checksum_header.zero = 0
    checksum_header.next_header = 6

    data_array = bytes(checksum_header) + bytes(tcp_header) + opts + data
    _sum = 0 
    for i in range(0, len(data_array), 2):
        a = data_array[i]
        try:
            b = data_array[i + 1]
        except IndexError:
            break
        _sum += a + (b << 8)
    _sum += _sum >> 16
    _sum = ~_sum & 0xffff
    _sum = socket.htons(_sum)

    return _sum


def unpack_tcp_header(buf: bytes) -> Tuple[int, int, Dict[str, bool], int, int, bytes]:
    header = TCPHeader()
    memmove(pointer(header), buf, sizeof(header))
    src = header.source_port
    dst = header.destination_port

    flags = {}
    flags['urg'] = bool(header.urg)
    flags['ack'] = bool(header.ack)
    flags['psh'] = bool(header.psh)
    flags['rst'] = bool(header.rst)
    flags['syn'] = bool(header.syn)
    flags['fin'] = bool(header.fin)

    data = buf[header.offset*4:]
    return src, dst, flags, int(header.sequence_number), int(header.checksum), data


def create_isn() -> int:
    return randint(0x0, 0xFFFF_FFFF)
