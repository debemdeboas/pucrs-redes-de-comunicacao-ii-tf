
from ctypes import *


PROTO = 17


class UDPHeader(BigEndianStructure):
    """
    RFC768. UDP packet header.
    ```
     0      7 8     15 16    23 24    31  
    +--------+--------+--------+--------+ 
    |     Source      |   Destination   | 
    |      Port       |      Port       | 
    +--------+--------+--------+--------+ 
    |                 |                 | 
    |     Length      |    Checksum     | 
    +--------+--------+--------+--------+ 
    ```
    """

    _fields_ = [
        ('src_port', c_uint16),
        ('dst_port', c_uint16),
        ('len', c_uint16),
        ('checksum', c_uint16)
    ]

    @staticmethod
    def create_and_add_ports(src_port, dst_port) -> 'UDPHeader':
        header = UDPHeader()
        header.src_port = src_port
        header.dst_port = dst_port
        return header


def create(src_port: int, dst_port: int, size: int, chksum: int = 0) -> UDPHeader:
    header = UDPHeader()
    header.src_port = src_port
    header.dst_port = dst_port
    header.len = size
    header.checksum = chksum
    return header


def unpack_header(buf: bytes) -> UDPHeader:
    hdr = UDPHeader()
    memmove(pointer(hdr), buf, sizeof(hdr))
    return hdr
