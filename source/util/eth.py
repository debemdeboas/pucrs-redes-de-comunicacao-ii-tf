
import ctypes
import netifaces
import socket
import struct

from typing import List, NewType, Tuple

from source.constants import NET_IFACE


ETH_FRAME_SIZE = 1518
ETH_P_ALL = 3
ETH_HEADER_FMT = '!6B6BH'
MAC_BROADCAST = 'FF:FF:FF:FF:FF:FF'

ETHHeader = NewType('Header', bytes)

def create_and_bind_socket(iface: str = NET_IFACE, port: int = 0) -> socket.SocketType:
    s = create_socket()
    s.bind((iface, port))
    return s


def create_socket() -> socket.SocketType:
    return socket.socket(
        socket.AF_PACKET,
        socket.SOCK_RAW,
        socket.htons(ETH_P_ALL)
    )


def mac_str_to_int_list(mac: str) -> list[int]:
    return [int(m, 16) for m in mac.split(':')]


def mac_to_byte_array(mac: list[int] | str) -> ctypes.Array:
    if isinstance(mac, str):
        mac = mac_str_to_int_list(mac)
    return (ctypes.c_ubyte * 16)(*mac)


def get_mac_data(iface: str) -> Tuple[List[int], str]:
    src_mac_data = netifaces.ifaddresses(iface)[netifaces.AF_LINK][0]
    src_mac_addr = [int(d, 16) for d in src_mac_data['addr'].split(':')]
    return (src_mac_addr, src_mac_data['addr'].upper())


def pack_eth_header(src: List[int], dst: List[int], packet_type: int) -> ETHHeader:
    return ETHHeader(struct.pack(ETH_HEADER_FMT, *dst, *src, packet_type))


def unpack_eth_header(buf: ETHHeader) -> Tuple[str, str, int]:
    if len(buf) > 14:
        buf = ETHHeader(buf[:14])
    data = struct.unpack(ETH_HEADER_FMT, buf)
    dst = ':'.join(f'{hex(b)[2:].upper():>02}' for b in data[:6])
    src = ':'.join(f'{hex(b)[2:].upper():>02}' for b in data[6:12])
    packet_type = int(data[12:14][0])
    return src, dst, packet_type
