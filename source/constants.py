
from ipaddress import IPv4Address

NET_IFACE = 'eth0'

LOCALHOST = IPv4Address('127.0.0.1')
BROADCAST = IPv4Address('255.255.255.255')
