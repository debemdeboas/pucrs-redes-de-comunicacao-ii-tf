
from ctypes import *
from ipaddress import IPv4Address, IPv4Network
from socket import htons, inet_ntoa

import netifaces
import struct

from source.constants import NET_IFACE


TYPE = 0x0800

class IPv4Header(BigEndianStructure):
    """
    RFC791. Represents an IPv4 packet header.

    ```
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |Version|  IHL  |Type of Service|          Total Length         |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |         Identification        |Flags|      Fragment Offset    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  Time to Live |    Protocol   |         Header Checksum       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                       Source Address                          |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Destination Address                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Options                    |    Padding    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ```
    """

    _fields_ = [
        ('version', c_uint8, 4),
        ('ihl', c_uint8, 4),
        ('tos', c_uint8),
        ('len', c_uint16),
        ('id', c_uint16),
        ('flags', c_ubyte, 3),
        ('offset', c_uint16, 13),
        ('ttl', c_uint8),
        ('protocol', c_uint8),
        ('checksum', c_uint16),
        ('src', c_uint32),
        ('dst', c_uint32),
    ]


def calc_checksum(hdr: IPv4Header):
    """
    Calculates an IPv4 header's checksum in-place.

    Args:
        hdr (IPv4Header)
    """
    data_array = bytes(hdr) #type: ignore
    _sum = 0
    for i in range(0, len(data_array), 2):
        a = data_array[i]
        try:
            b = data_array[i + 1]
        except IndexError:
            break
        _sum += a + (b << 8)
    _sum += _sum >> 16
    _sum = ~_sum & 0xffff
    _sum = htons(_sum)
    hdr.checksum = _sum


def get_our_ip_and_net_mask(iface: str = NET_IFACE) -> tuple[str, str]:
    addr_info = netifaces.ifaddresses(iface)[netifaces.AF_INET][0]
    return addr_info['addr'], addr_info['netmask']


def get_address_pool(ip: str | IPv4Address, mask: str | IPv4Address) -> IPv4Network:
    if isinstance(ip, str):
        ip = IPv4Address(ip)
    if isinstance(mask, str):
        mask = IPv4Address(mask)
    network = IPv4Address(int.from_bytes(ip.packed)&int.from_bytes(mask.packed))
    return IPv4Network(f'{network.exploded}/{mask.exploded}')


def get_default_gateway(iface: str) -> IPv4Address:
    with open('/proc/net/route') as f:
        for line in f:
            fields = line.strip().split()
            if fields[0].lower() != iface:
                continue
            if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue
            return IPv4Address(inet_ntoa(struct.pack('<L', int(fields[2], 16))))
    raise Exception(f'Could not find default gateway for interface {iface}')


def unpack_header(buf: bytes) -> IPv4Header:
    hdr = IPv4Header()
    memmove(pointer(hdr), buf, sizeof(hdr))
    return hdr
