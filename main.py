
import logging
import random
import socket

from ctypes import sizeof
from ipaddress import IPv4Address
from rich.console import Console
from rich.logging import RichHandler
from typing import Any

from source.constants import NET_IFACE, LOCALHOST, BROADCAST
from source.util import udp, ipv4, eth, dhcp


# GET MAC DATA
OUR_MAC, OUR_MAC_STR = eth.get_mac_data(NET_IFACE)

# GET IP AND MASK DATA
OUR_IP, OUR_NETMASK = ipv4.get_our_ip_and_net_mask()

# USE OUR IP AS DEFAULT GATEWAY
# We could also use ipv4.get_default_gateway(NET_IFACE) to
# get this computer's already confgured default gateway.
DEFAULT_GATEWAY = OUR_IP

# GET THE `dnsmasq` CONTAINER'S IP ADDRESS (THANKS TO DOCKER'S OWN DNS SERVER)
DNS_SERVERS = [socket.gethostbyname('dnsmasq')]

# ALLOCATE IP ADDRESS POOL
OUR_NETWORK = ipv4.get_address_pool(OUR_IP, OUR_NETMASK)
hosts_range = list(OUR_NETWORK.hosts())[15:-10]
allocatable_pool = {k: dhcp.DHCPIPStatus.FREE for k in hosts_range}
allocatable_pool[IPv4Address(OUR_IP)] = dhcp.DHCPIPStatus.TAKEN

# ALLOCATE DHCP EXCHANGES (XID) DICTIONARY
curr_exchanges: dict[int, Any] = {}

# CREATE SOCKET-BY-INTERFACE DICTIONARY
# This dictioanry is in the form <interface_name, socket> and
# the sockets are allocated dynamically whenever a DHCP packet
# from a previously unknown interface gets captured.
socks: dict[str, socket.SocketType] = {}

# CREATE A RAW SOCKET THAT LISTENS ON ALL INTERFACES
# This socket listens on all available interfaces on this machine.
# However, this socket CANNOT send any packets since it's not *bound*
# to any interface.
all_iface_sock = eth.create_socket()
all_iface_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
all_iface_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# CREATE LOGGER
logging.basicConfig(
    level='NOTSET', format='%(message)s', datefmt='[%X]', handlers=[RichHandler(
        console=Console(no_color=False),
        omit_repeated_times=False
    )]
)
log = logging.getLogger('main')
log.debug('Logger created')

# LOG INFORMATION
log.info('### DHCP SERVER READY ###')
log.info(f'Running on {OUR_IP} at {OUR_NETWORK}')
log.info(f'DHCP range: {hosts_range[0]} to {hosts_range[-1]}')

# START SERVER
while True:
    try:
        # WAIT FOR PACKETS
        data, iface_info = all_iface_sock.recvfrom(4096)

        nstart = 14
        src_mac, dst_mac, packet_type = eth.unpack_eth_header(eth.ETHHeader(data[:nstart]))

        if packet_type != ipv4.TYPE:
            # NOT IPV4
            continue

        rcvd_ip_hdr = ipv4.unpack_header(data[nstart:])
        nstart += sizeof(rcvd_ip_hdr)

        if rcvd_ip_hdr.protocol != udp.PROTO:
            # NOT UDP
            continue

        if IPv4Address(rcvd_ip_hdr.dst) != IPv4Address(OUR_IP) and \
           IPv4Address(rcvd_ip_hdr.dst) != BROADCAST:
           # NOT FOR US
            continue

        rcvd_udp_hdr = udp.unpack_header(data[nstart:])
        nstart += sizeof(rcvd_udp_hdr)

        if rcvd_udp_hdr.src_port != dhcp.DHCP_CLIENT_PORT:
            # (PROBABLY) NOT A DHCP MESSAGE
            continue

        rcvd_dhcp_msg, options = dhcp.unpack_msg(data[nstart:])

        if rcvd_dhcp_msg.op != 1 or not options:
            # EITHER ANOTHER SERVER'S MESSAGE OR INVALID DHCP MESSAGE (NO OPTIONS)
            continue

        # GET THE CURRENT INTERFACE'S SOCKET
        iface = iface_info[0]
        if iface not in socks: # IF THE PACKET'S INTERFACE IS NEW, CREATE ITS SOCKET
            # NEW INTERFACE! CREATE SOCKET
            sock = eth.create_socket()
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # BIND THE NEW SOCKET TO THE CORRESPONDING INTERFACE ON PORT `DHCP_SERVER_PORT`
            sock.bind((iface, dhcp.DHCP_SERVER_PORT))
            # ADD THE SOCKET OBJECT TO THE DICTIONARY
            socks[iface] = sock

        # NO IF/ELSE BLOCKS TO MINIMIZE BRANCHING
        sock = socks[iface]

        # HANDLE DHCP MESSAGE
        payload = b''
        match options.get(dhcp.DHCP_OPTION_MESSAGE_TYPE, -1):
            case dhcp.DHCPDISCOVER:
                their_ip = dhcp.allocate_ip(allocatable_pool)
                allocatable_pool[their_ip] = dhcp.DHCPIPStatus.OFFERED

                curr_exchanges[rcvd_dhcp_msg.xid] = {
                    'status':     'DISCOVER SENT',
                    'ip':         their_ip,
                    'client_mac': src_mac
                }

                dhcp_answer = dhcp.offer(
                    rcvd_dhcp_msg,
                    options,
                    OUR_IP,
                    OUR_NETMASK,
                    DEFAULT_GATEWAY,
                    DNS_SERVERS,
                    their_ip
                )

                udp_hdr = udp.create(
                    dhcp.DHCP_SERVER_PORT,
                    dhcp.DHCP_CLIENT_PORT,
                    sizeof(udp.UDPHeader) + len(dhcp_answer)
                )

                ipv4_hdr          = ipv4.IPv4Header()
                ipv4_hdr.version  = 4
                ipv4_hdr.ihl      = 5
                ipv4_hdr.tos      = 0
                ipv4_hdr.len      = (ipv4_hdr.ihl * 4) + udp_hdr.len
                ipv4_hdr.id       = random.randint(1, 0xffff-1)
                ipv4_hdr.flags    = 0
                ipv4_hdr.offset   = 0
                ipv4_hdr.ttl      = 0xffff
                ipv4_hdr.protocol = udp.PROTO
                ipv4_hdr.checksum = 0
                ipv4_hdr.src      = int(IPv4Address(OUR_IP))
                ipv4_hdr.dst      = int(BROADCAST)
                ipv4_hdr.options  = 0
                ipv4_hdr.padding  = 0
                ipv4.calc_checksum(ipv4_hdr)

                eth_hdr = eth.pack_eth_header(OUR_MAC, [int(d, 16) for d in eth.MAC_BROADCAST.split(':')], ipv4.TYPE)

                payload = eth_hdr + ipv4_hdr + udp_hdr + dhcp_answer
                log.info(f'{curr_exchanges[rcvd_dhcp_msg.xid]["status"]} to {src_mac}. Allocated IP {their_ip}')

            case dhcp.DHCPREQUEST:
                xid_info = curr_exchanges[rcvd_dhcp_msg.xid]
                their_ip = xid_info['ip']

                xid_info['status'] = 'ACK SENT'
                allocatable_pool[their_ip] = dhcp.DHCPIPStatus.TAKEN

                dhcp_answer = dhcp.ack(
                    rcvd_dhcp_msg,
                    options,
                    OUR_IP,
                    OUR_NETMASK,
                    DEFAULT_GATEWAY,
                    DNS_SERVERS,
                    their_ip
                )

                udp_hdr = udp.create(
                    dhcp.DHCP_SERVER_PORT,
                    dhcp.DHCP_CLIENT_PORT,
                    sizeof(udp.UDPHeader) + len(dhcp_answer)
                )

                ipv4_hdr          = ipv4.IPv4Header()
                ipv4_hdr.version  = 4
                ipv4_hdr.ihl      = 5
                ipv4_hdr.tos      = 0
                ipv4_hdr.len      = (ipv4_hdr.ihl * 4) + udp_hdr.len
                ipv4_hdr.id       = random.randint(1, 0xffff-1)
                ipv4_hdr.flags    = 0
                ipv4_hdr.offset   = 0
                ipv4_hdr.ttl      = 0xffff
                ipv4_hdr.protocol = udp.PROTO
                ipv4_hdr.checksum = 0
                ipv4_hdr.src      = int(IPv4Address(OUR_IP))
                ipv4_hdr.dst      = int(BROADCAST)
                ipv4_hdr.options  = 0
                ipv4_hdr.padding  = 0
                ipv4.calc_checksum(ipv4_hdr)

                eth_hdr = eth.pack_eth_header(OUR_MAC, [int(d, 16) for d in eth.MAC_BROADCAST.split(':')], ipv4.TYPE)

                payload = eth_hdr + ipv4_hdr + udp_hdr + dhcp_answer
                log.info(f'{curr_exchanges[rcvd_dhcp_msg.xid]["status"]} to {xid_info["client_mac"]}. Allocated IP {their_ip}')

            case dhcp.DHCPDECLINE | dhcp.DHCPRELEASE:
                xid_info = curr_exchanges[int.from_bytes(rcvd_dhcp_msg.xid, 'big', signed=False)]
                allocatable_pool[xid_info['ip']] = dhcp.DHCPIPStatus.FREE
                curr_exchanges.pop(curr_exchanges[int.from_bytes(rcvd_dhcp_msg.xid, 'big', signed=False)])

            case dhcp.DHCPOFFER | dhcp.DHCPACK | dhcp.DHCPNACK:
                log.info('Received server-side message. Ignoring')
                continue

            case -1 | _:
                log.warn('Unknown DHCP message type')
                continue

        # SEND PAYLOAD TO CLIENT
        sock.send(payload)
    except KeyboardInterrupt:
        break
    except dhcp.DHCPError as e:
        log.warning(e)
    except KeyError:
        continue
    except Exception as e:
        continue

log.info('DHCP server shutting down.')
